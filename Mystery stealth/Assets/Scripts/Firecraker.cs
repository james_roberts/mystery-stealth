﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firecraker : Weapon {

    public float force;
    
    public float soundRingRange;

    public override void PlayAnimation()
    {
        
    }

    public override void UseWeapon()
    {
        this.gameObject.GetComponent<PhotonView>().RPC("Throw", PhotonTargets.All);
        GUIManager.Instance.ChangeHeldItem(null);
    }

    [PunRPC]
    void Throw()
    {
        owner.GetComponent<PlayerControls>().animator.SetTrigger("Drop");
        this.GetComponent<Rigidbody>().isKinematic = false;
        SetOwner(null);
        this.GetComponent<BoxCollider>().isTrigger = false;
        this.GetComponent<Rigidbody>().AddForce(this.transform.parent.forward * force, ForceMode.Impulse);
        this.transform.parent = null;
        IsFired = true;

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (IsFired)
        {
            this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, soundRingRange, this.GetComponent<PhotonView>().viewID);
            Destroy(this.gameObject);
        }
    }
}
