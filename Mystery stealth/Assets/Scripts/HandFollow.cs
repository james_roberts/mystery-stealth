﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandFollow : MonoBehaviour {

    public GameObject bone;
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(bone.transform.position.x - 0.007f, bone.transform.position.y, bone.transform.position.z);
        this.transform.rotation = Quaternion.Euler(transform.parent.rotation.eulerAngles.x, transform.parent.rotation.eulerAngles.y, transform.parent.rotation.eulerAngles.z);
    }
}
