﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PhotonView))]
abstract public class Weapon : Photon.MonoBehaviour {

    public int damageAmount;
    public GameObject soundRingPrefab;

    private bool isFired = false;
    protected bool hit = false;

    protected GameObject owner;
    public Sprite image;
    public AudioClip clip;
    public GameObject tooltipCanvas;

    public bool IsFired
    {
        get
        {
            return isFired;
        }

        protected set
        {
            isFired = value;
        }
    }

    protected virtual void Update()
    {
        if (this.transform.parent != null && this.transform.parent.tag == "Player")
        {
            if (owner.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).IsTag("Idle"))
            {
                IsFired = false;
                hit = false;
            }
            else if (owner.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).IsTag("Use"))
            {
                IsFired = true;
            }
        }
    }

    public abstract void UseWeapon();

    public virtual void PlayAnimation()
    {
    }

    public int getDamageAmount()
    {
        return damageAmount;
    }

    public void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    public GameObject GetOwner()
    {
        return owner;
    }

    //Creates a sound ring and plays given clip.
    [PunRPC]
    public void Sound(float maxRadius, int objId)
    {
        GameObject obj = PhotonView.Find(objId).gameObject;

        GameObject SoundRing = Instantiate(soundRingPrefab, obj.transform.position, soundRingPrefab.transform.rotation);
        SoundRing.GetComponent<Ring>().maxRadius = maxRadius;
        SoundRing.GetComponent<Ring>().SetSound(clip);

        if (this.gameObject == GameManager.Instance.LocalPlayer)
        {
            if (maxRadius <= 10.0f)
            {
                SoundRing.GetComponent<Ring>().SetColour(Ring.Colours.Green);
            }
            else
            {
                SoundRing.GetComponent<Ring>().SetColour(Ring.Colours.Yellow);
            }
        }
        else
        {
            SoundRing.GetComponent<Ring>().SetColour(Ring.Colours.Red);
        }
    }

    //Enable or disable canvas for tooltip
    public void DisplayTooltip()
    {
        tooltipCanvas.SetActive(!tooltipCanvas.GetActive());
    }
}
