﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : Photon.MonoBehaviour {

    private static GUIManager instance;
    public static GUIManager Instance
    {
        get
        {
            return instance;
        }
    }

    [Header("Minimap")]
    public RenderTexture miniMapTexture;
    public Material miniMapMaterial;
    public float offset;

    [Header("Console")]
    public ScrollRect scollWindow;
    public GameObject console;
    public Text textbox;

    [Header("UI")]
    public GameObject pauseMenu;
    public Image heldItemImage;
    public Text weaponNameText;
    public GameObject controlsMenu;
    public Slider healthBar;

    private void Start()
    {
        instance = this;

        //UI health bar
        healthBar.maxValue = GameManager.Instance.LocalPlayer.GetComponent<PlayerHealth>().maxHealth;
        healthBar.value = GameManager.Instance.LocalPlayer.GetComponent<PlayerHealth>().maxHealth;
    }

    private void Update()
    {
        //Console
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C))
        {
            scollWindow.gameObject.SetActive(!scollWindow.gameObject.activeSelf);
        }

        //Pause Menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(true);
            GameManager.Instance.LocalPlayer.GetComponent<PlayerControls>().canControl = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    void OnGUI () {
        if(Event.current.type == EventType.Repaint)
            Graphics.DrawTexture(new Rect(Screen.width - 256 - offset, Screen.height - 256 - offset, 256, 256), miniMapTexture, miniMapMaterial);
	}

    public void Log(string message)
    {
        Text text = Instantiate(textbox, console.transform, false);

        text.text = " " + message;

        scollWindow.verticalNormalizedPosition = 0.0f;
    }

    //Pause button Quit.
    public void Quit()
    {
        Application.Quit();
    }

    //End button Replay.
    public void Replay()
    {
        GameObject.Find("Network Manager").GetComponent<NetworkManager>().StartGame();
    }

    //Pause button Resume.
    public void Resume()
    {
        pauseMenu.SetActive(false);
        GameManager.Instance.LocalPlayer.GetComponent<PlayerControls>().canControl = true;
        Cursor.visible = false;
    }

    //Pause button Leave Game.
    public void LeaveGame()
    {
        LeaveRoom();

        //Delete player object from all clients.
        GameManager.Instance.gameObject.GetPhotonView().RPC("DeletePlayer", PhotonTargets.All, GameManager.Instance.LocalPlayer.GetPhotonView().viewID);
    }

    //End game menu Leave Game
    public void LeaveRoom()
    {
        //Leave room
        this.gameObject.GetPhotonView().RPC("Leave", PhotonTargets.All);
    }

    [PunRPC]
    void Leave()
    {
        //Have all clients leave room
        PhotonNetwork.LeaveRoom();
    }

    /*
     * Display sprite on UI.
     */
    public void ChangeHeldItem(Sprite itemImage)
    {
        heldItemImage.overrideSprite = itemImage;

        if (itemImage != null)
            weaponNameText.text = itemImage.name;
        else
            weaponNameText.text = "";
    }

    /*
     * Display Controls menu from Pause menu
     */
     public void ControlsMenu()
    {
        pauseMenu.SetActive(!pauseMenu.GetActive());
        controlsMenu.SetActive(!controlsMenu.GetActive());
    }

    /*
     * Update UI health bar
     */
     public void UpdateHealthBar(int health)
    {
        healthBar.value = health;
    }
}
