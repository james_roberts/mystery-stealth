﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorControls : Photon.MonoBehaviour
{
    float xAxis;
    float yAxis;
    float zAxis;

    public float movementSpeed;

    [HideInInspector]
    public bool canControl = true;

    [SerializeField]
    Behaviour[] components;

    private void Start()
    {
        //Display player UI
        GameObject.Find("GUIManager").SetActive(false);
        GameObject.Find("Canvas").transform.FindChild("Health Overlay").gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (!canControl)
            return;

        xAxis = Input.GetAxis("Horizontal") * Time.deltaTime * movementSpeed;
        yAxis = Input.GetAxis("Height") * Time.deltaTime * movementSpeed;
        zAxis = Input.GetAxis("Vertical") * Time.deltaTime * movementSpeed;

        transform.Translate(xAxis, yAxis, zAxis);
    }
}
