﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : Weapon {

    [PunRPC]
    public override void PlayAnimation()
    {
        owner.GetComponent<Animator>().SetTrigger("Stab");
    }

    public override void UseWeapon()
    {
        IsFired = true;
        
        this.gameObject.GetComponent<PhotonView>().RPC("PlayAnimation", PhotonTargets.All);

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 4.0f))
        {
            if (hit.collider.tag == "Player_back")
            {
                hit.collider.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.transform.parent.gameObject.GetPhotonView().viewID);
            } else if (hit.collider.tag == "Player")
            {
                //If hit any other part of player, less damage
                hit.collider.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount / 3, hit.collider.transform.parent.gameObject.GetPhotonView().viewID);
            }
        }
    }
}
