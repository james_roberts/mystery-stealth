﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloakingDevice : Weapon {

    public override void UseWeapon()
    {
        owner.GetComponent<PlayerControls>().cloaked = true;
        GUIManager.Instance.ChangeHeldItem(null);
        this.photonView.RPC("DestoryObject", PhotonTargets.All);
    }

    [PunRPC]
    void DestoryObject()
    {
        owner.GetComponent<PlayerControls>().animator.SetTrigger("Drop");
        Destroy(gameObject);
    }
}
