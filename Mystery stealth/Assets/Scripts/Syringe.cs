﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Syringe : Weapon
{
    [PunRPC]
    public override void PlayAnimation()
    {
        //this.transform.parent.GetComponent<Animator>().SetTrigger("Use Stab");
        owner.GetComponent<Animator>().SetTrigger("Stab");
    }

    public override void UseWeapon()
    {
        IsFired = true;
        Debug.Log("Use Syringe");
        this.gameObject.GetComponent<PhotonView>().RPC("PlayAnimation", PhotonTargets.All);

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 4.0f))
        {
            if (hit.collider.tag == "Player")
            {
                hit.collider.transform.GetComponent<PhotonView>().RPC("Ill", PhotonTargets.All, hit.collider.gameObject.GetPhotonView().viewID);
            }
            else if (hit.collider.tag == "Player_back")
            {
                hit.collider.transform.parent.GetComponent<PhotonView>().RPC("Ill", PhotonTargets.All, hit.collider.transform.parent.gameObject.GetPhotonView().viewID);
            }
        }
    }
}
