﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon {

    public int ammo;
    public float soundRingRange;

    [PunRPC]
    public override void PlayAnimation()
    {
        //this.transform.parent.GetComponent<Animator>().SetTrigger("Use Shot");
        owner.GetComponent<Animator>().SetTrigger("Shot");
    }

    public override void UseWeapon()
    {
        if (ammo != 0)
        {
            IsFired = true;

            Debug.Log("Use Gun");

            this.gameObject.GetComponent<PhotonView>().RPC("PlayAnimation", PhotonTargets.All);

            this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, soundRingRange, this.GetComponent<PhotonView>().viewID);

            ammo--;

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 50.0f))
            {
                if (hit.collider.tag == "Player")
                {
                    hit.collider.transform.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.gameObject.GetPhotonView().viewID);
                }
                else if (hit.collider.tag == "Player_back")
                {
                    hit.collider.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.transform.parent.gameObject.GetPhotonView().viewID);
                }
            }
        }
    }
}
