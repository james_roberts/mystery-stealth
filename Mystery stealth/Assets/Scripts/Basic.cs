﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basic : Weapon
{
    public int force;

    [PunRPC]
    public override void PlayAnimation()
    {
    }

    public override void UseWeapon()
    {
        this.GetComponent<PhotonView>().RPC("Throw", PhotonTargets.All);
        GUIManager.Instance.ChangeHeldItem(null);
    }

    [PunRPC]
    void Throw()
    {
        owner.GetComponent<PlayerControls>().animator.SetTrigger("Drop");
        this.gameObject.GetComponent<Rigidbody>().isKinematic = false; //Turn on rigidbody
        this.gameObject.GetComponent<BoxCollider>().isTrigger = false; //Turn on collider
        this.gameObject.transform.parent = null;                       //Remove object from hand

        IsFired = true;
        this.gameObject.GetComponent<Rigidbody>().AddForce(owner.transform.forward * force, ForceMode.Impulse);
        StartCoroutine(Reset()); //Reset weapon for reuse.
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (IsFired) //If object hits something after being thrown, play sound.
        {
            Sound(15.0f, this.GetComponent<PhotonView>().viewID);
            //this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15, this.GetComponent<PhotonView>().viewID);
        }
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(3.0f);

        IsFired = false;
        hit = false;
        owner = null;
    }
}
