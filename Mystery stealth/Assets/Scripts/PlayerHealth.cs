﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerHealth : Photon.MonoBehaviour
{
    public int health;
    public int maxHealth = 10;

    public Slider hoverHealthBar;

    // Use this for initialization
    void Start () {
        health = maxHealth;
        hoverHealthBar.maxValue = maxHealth;
        hoverHealthBar.value = maxHealth;
    }

    [PunRPC]
    public void Damage(int damage, int playerId)
    {
        GameObject player = PhotonView.Find(playerId).gameObject;

        //Hover health bar
        player.GetComponent<PlayerHealth>().health -= damage;
        player.GetComponent<PlayerHealth>().hoverHealthBar.value = health;

        //UI health bar, If player taking damage is the local player, update UI overlay health bar
        if(GameManager.Instance.LocalPlayer == player)
            GUIManager.Instance.UpdateHealthBar(health);

        if (health <= 0)
        {
            player.gameObject.GetComponent<PlayerControls>().Sound(30.0f, "Death", player.GetComponent<PhotonView>().viewID);
            GameManager.Instance.Death(this.gameObject);
        }
        else
        {
            player.gameObject.GetComponent<PlayerControls>().Sound(20.0f, "Pain", player.GetComponent<PhotonView>().viewID);
        }
 
        Debug.Log("Player " + player.name + " Lost health " + health);
    }

    [PunRPC]
    public void Ill(int playerId)
    {
        GameObject player = PhotonView.Find(playerId).gameObject;

        //If player object belongs to client
        if (player.GetPhotonView().isMine)
        {
            StartCoroutine(GameManager.Instance.FadeOutOverlay());
        }
    }
}
