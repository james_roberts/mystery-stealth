using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour
{
	[Header("Door Settings")]
	public float initialAngle = 0.0F;
	public float rotationAngle = 90.0F;

	public enum PositionOfHinge
	{
		Left,
		Right,
        Up,
	}
	public PositionOfHinge hingePosition;

	public enum SideOfRotation
	{
		Left,
		Right,
        Up,
	}
	public SideOfRotation rotationSide;
    
	public float rotationSpeed = 3F;
	public int timesMoveable = 0;

	private int n = 0; //For 'TimesMoveable' loop.
	[HideInInspector] public bool isRunning = false;

	//Define an initial and final rotation.
	private Quaternion finalRot, initialRot;
	private int currentState;

	//Create a hinge.
	GameObject hinge;
    
	void Start ()
	{
		//Give the object the tag "Door" for future reference.
		gameObject.tag = "Door";

		//Create a hinge.
		hinge = new GameObject();
		hinge.name = "hinge";

		//Calculate sine/cosine of initial angle (needed for hinge positioning).
		float CosDeg = Mathf.Cos ((transform.eulerAngles.y * Mathf.PI) / 180);
		float SinDeg = Mathf.Sin ((transform.eulerAngles.y * Mathf.PI) / 180);

		//Read transform (position/rotation/scale) of the door.
		float PosDoorX = transform.position.x;
		float PosDoorY = transform.position.y;
	    float PosDoorZ = transform.position.z;

		float RotDoorX = transform.localEulerAngles.x;
		float RotDoorZ = transform.localEulerAngles.z;

		float ScaleDoorX = transform.localScale.x;
		float ScaleDoorZ = transform.localScale.z;
        float ScaleDoorY = transform.localScale.y;

        //Create a placeholder of the hinge's position/rotation.
        Vector3 HingePosCopy = hinge.transform.position;
		Vector3 HingeRotCopy = hinge.transform.localEulerAngles;

		//Left Position
		if (hingePosition == PositionOfHinge.Left)
		{
            //Calculate hinge location
            if (transform.localScale.x > transform.localScale.z)
			{
				HingePosCopy.x = (PosDoorX - (ScaleDoorX / 2 * CosDeg));
				HingePosCopy.z = (PosDoorZ + (ScaleDoorX / 2 * SinDeg));
				HingePosCopy.y = PosDoorY;

				HingeRotCopy.x = RotDoorX;
				HingeRotCopy.y = -initialAngle;
				HingeRotCopy.z = RotDoorZ;
			}

			else
			{
				HingePosCopy.x = (PosDoorX + (ScaleDoorZ / 2 * CosDeg));
				HingePosCopy.z = (PosDoorZ + (ScaleDoorZ / 2 * SinDeg));
				HingePosCopy.y = PosDoorY;

				HingeRotCopy.x = RotDoorX;
				HingeRotCopy.y = -initialAngle;
				HingeRotCopy.z = RotDoorZ;
     		}
		}

        //Right Position
        if (hingePosition == PositionOfHinge.Right)
		{
            //Calculate hinge location
            if (transform.localScale.x > transform.localScale.z)
			{
				HingePosCopy.x = (PosDoorX + (ScaleDoorX / 2 * CosDeg));
				HingePosCopy.z = (PosDoorZ - (ScaleDoorX / 2 * SinDeg));
				HingePosCopy.y = PosDoorY;

				HingeRotCopy.x = RotDoorX;
				HingeRotCopy.y = -initialAngle;
				HingeRotCopy.z = RotDoorZ;
			}

			else
            {
				HingePosCopy.x = (PosDoorX - (ScaleDoorZ / 2 * CosDeg));
				HingePosCopy.z = (PosDoorZ - (ScaleDoorZ / 2 * SinDeg));
				HingePosCopy.y = PosDoorY;

				HingeRotCopy.x = RotDoorX;
				HingeRotCopy.y = -initialAngle;
				HingeRotCopy.z = RotDoorZ;
			}
		}

        //Up Position
        if (hingePosition == PositionOfHinge.Up)
        {
            //Calculate hinge location
            if (transform.localScale.x > transform.localScale.z)
            {
                HingePosCopy.x = PosDoorX;
                HingePosCopy.z = PosDoorZ;
                HingePosCopy.y = (PosDoorY + (ScaleDoorY));

                HingeRotCopy.x = RotDoorX;
                HingeRotCopy.y = -initialAngle;
                HingeRotCopy.z = RotDoorZ;
            }
            else
            {
                HingePosCopy.x = PosDoorX;
                HingePosCopy.z = PosDoorZ;
                HingePosCopy.y = (PosDoorY + (ScaleDoorY * 4));

                HingeRotCopy.x = RotDoorX;
                HingeRotCopy.y = -initialAngle;
                HingeRotCopy.z = RotDoorZ;
            }
        }

        //Create door hinge by creating a pivot point
        hinge.transform.position = HingePosCopy;
		transform.parent = hinge.transform;
		hinge.transform.localEulerAngles = HingeRotCopy;
        
        //Get initial rotation and final rotation based on selected SideOfRotation.
		if (rotationSide == SideOfRotation.Left)
		{
			initialRot = Quaternion.Euler (0, -initialAngle, 0);
			finalRot = Quaternion.Euler(0, -initialAngle - rotationAngle, 0);
		}

		if (rotationSide == SideOfRotation.Right)
		{
			initialRot = Quaternion.Euler (0, -initialAngle, 0);
			finalRot = Quaternion.Euler(0, -initialAngle + rotationAngle, 0);
		}

        if (rotationSide == SideOfRotation.Up)
        {
            initialRot = Quaternion.Euler(0, -initialAngle, 0);
            finalRot = Quaternion.Euler(0, 0, -initialAngle + rotationAngle);
        }
    }

    [PunRPC]
    public void Open()
    {
        StartCoroutine(OpenAnimation());
    }
    
	IEnumerator OpenAnimation ()
    {
		if (n < timesMoveable || timesMoveable == 0)
		{
			//Change state from 1 to 0 and back (= alternate between FinalRot and InitialRot).
			if (hinge.transform.rotation == (currentState == 0 ? finalRot : initialRot))
                currentState ^= 1;

			//Set 'FinalRotation' to 'FinalRot' when moving and to 'InitialRot' when moving back.
			Quaternion FinalRotation = ((currentState == 0) ? finalRot : initialRot);

    	    //Make the door/window rotate until it is fully opened/closed.
    	    while (Mathf.Abs(Quaternion.Angle(FinalRotation, hinge.transform.rotation)) > 0.01f)
    	    {
			    isRunning = true;
			    hinge.transform.rotation = Quaternion.Lerp (hinge.transform.rotation, FinalRotation, Time.deltaTime * rotationSpeed);

            yield return new WaitForEndOfFrame();
    	    }

			isRunning = false;

			if (timesMoveable == 0)
                n = 0;
            else
                n++;
		}
	}
}
