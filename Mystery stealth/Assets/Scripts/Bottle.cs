﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottle : Weapon {

    [PunRPC]
    public override void PlayAnimation()
    {
        //this.transform.parent.GetComponent<Animator>().SetTrigger("Use Stab");
        owner.GetComponent<Animator>().SetTrigger("Stab");
    }

    public override void UseWeapon()
    {
        IsFired = true;
        
        this.gameObject.GetComponent<PhotonView>().RPC("PlayAnimation", PhotonTargets.All);

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 4.0f))
        {
            if (hit.collider.tag == "Player")
            {
                hit.collider.transform.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.gameObject.GetPhotonView().viewID);
            }
            else if (hit.collider.tag == "Player_back")
            {
                hit.collider.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.transform.parent.gameObject.GetPhotonView().viewID);
            }

            if (this.transform.GetChild(0).gameObject.GetActive())//If first child is active, change meshes.
            {
                this.gameObject.GetComponent<PhotonView>().RPC("SmashBottle", PhotonTargets.All);
            }
        }
    }

    //Change bottle mesh
    [PunRPC]
    void SmashBottle()
    {
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(true);
    }
}
