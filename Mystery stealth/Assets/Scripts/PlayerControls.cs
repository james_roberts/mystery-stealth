﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(CharacterController))]
public class PlayerControls : Photon.MonoBehaviour {

    [Header("Movement")]
    [SerializeField] private float moveSpeed = 2;
    private float storeMoveSpeed;
    [SerializeField] private float jumpForce = 4;
    private float storeJumpForce;
    [SerializeField] private float gravity = 20.0F;
    public Animator animator;

    private float currentV = 0;
    private float currentH = 0;

    private float interpolation = 10;
    private float runScale = 1.66f;
    private float backwardsWalkScale = 0.30f;
    private float backwardRunScale = 0.66f;

    private bool wasGrounded;
    private Vector3 moveDirection = Vector3.zero;

    Vector3 networkPosition;
    Quaternion networkRotation;
    float networkSmoothing = 5f;

    [HideInInspector]
    public bool canControl = false;

    [Header("GameObjects")]

    public GameObject hand;
    public Camera mainCamera;
    public Camera minimapCamera;
    public GameObject marker;
    public GameObject torch;
    
    [Header("Sound")]

    public GameObject soundRingPrefab;
    public AudioClip footstep1;
    public AudioClip footstep2;
    public AudioClip audioPain;
    public AudioClip audioDeath;
    public AudioClip audioWhistle;
    public AudioClip audioBreath;

    public AudioSource audioPlayerFootstep;
    
    private float audioTimer;
    private float audioMaxTimer;
    private int footstepCounter;
    float idleTimer = 0.0f;

    CharacterController controller;
    
    bool crouching = false;
    bool crouchClear = true;

    GameObject hoverOverObject;
    Shader standardShader;
    Shader highlightShader;

    [HideInInspector]
    public bool cloaked = false;

    // Use this for initialization
    void Start () {
        if (!photonView.isMine)
        {
            animator = GetComponent<Animator>(); //If not current player, get the object's animator.
            this.gameObject.GetComponent<CharacterController>().enabled = false;
            return;
        }
        
        audioMaxTimer = footstep1.length;
        audioTimer = audioMaxTimer;
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        marker.SetActive(true);
        this.gameObject.transform.FindChild("Character").FindChild("C_man_1_FBX2013").GetComponent<SkinnedMeshRenderer>().enabled = false;
        standardShader = Shader.Find("Standard");
        highlightShader = Shader.Find("Outlined/Silhouetted Bumped Diffuse");

        storeMoveSpeed = moveSpeed;
        storeJumpForce = jumpForce;
    }

    void FixedUpdate()
    {
        if (!photonView.isMine)
            return;

        if (!canControl)
            return;

        audioTimer += Time.deltaTime;

        //Move player
        MoveUpdate();
    }

    private void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, networkPosition, Time.deltaTime * networkSmoothing);
            transform.rotation = Quaternion.Lerp(transform.rotation, networkRotation, Time.deltaTime * networkSmoothing);
            return;
        }

        if (!canControl)
            return;

        //E key
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (hand.transform.childCount == 0) //If not holding weapon
            {
                Detect();
            }
            else //If holding weapon
            {
                this.GetComponent<PhotonView>().RPC("Drop", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);

                GUIManager.Instance.ChangeHeldItem(null);
            }
        }

        //Run
        if (!crouching)
        {
            if (Input.GetKeyDown(KeyCode.LeftShift) && controller.isGrounded)
            {
                audioMaxTimer = footstep1.length / 2;
                audioPlayerFootstep.pitch = 1.5f;
                moveSpeed = moveSpeed * 1.3f;
            }
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                audioMaxTimer = footstep1.length;
                audioPlayerFootstep.pitch = 1f;
                moveSpeed = storeMoveSpeed;
            }
        }

        //Crouch
        if (Input.GetKeyDown(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftShift) && !crouching) //If Left Control is presses and not already crouching or running
        {
            Crouch();
        }

        if (crouching) //If crouching check to see if space above to stand
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.up, out hit, 3.0f))
            {
                crouchClear = false;
            } else
            {
                crouchClear = true;
            }
        }

        if (crouching && crouchClear && !Input.GetKey(KeyCode.LeftControl)) //If Left Control is not being held, player is crouching and there is space to stand
        {
            Stand();
        }

        //Turn on/off torch.
        if (Input.GetKeyDown(KeyCode.F))
        {
            //If disabled, enable, otherwise disable
            if(torch.GetActive() == false)
                this.GetComponent<PhotonView>().RPC("Torch", PhotonTargets.All, true);
            else
                this.GetComponent<PhotonView>().RPC("Torch", PhotonTargets.All, false);
        }

        //Highlight weapons on hover over.
        RaycastHit rayHit;
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out rayHit, 3))
            {
                if (rayHit.collider.tag == "Weapon")
                { //If object is a weapons and not owned.
                    if (rayHit.collider.gameObject.GetComponent<Weapon>().GetOwner() == null)
                    {
                        if (rayHit.collider.gameObject != hoverOverObject)//If the object found is not already highlighted...
                        {
                            rayHit.collider.transform.GetChild(0).GetComponentInChildren<Renderer>().material.shader = highlightShader;
                            hoverOverObject = rayHit.collider.gameObject;
                            hoverOverObject.GetComponent<Weapon>().DisplayTooltip();
                        }
                    }
                }
            } 
            //No longer hovering over.
            if(hoverOverObject != null && (rayHit.collider == null || hoverOverObject != rayHit.collider.gameObject))
            {
                hoverOverObject.transform.GetChild(0).GetComponentInChildren<Renderer>().material.shader = standardShader;
                hoverOverObject.GetComponent<Weapon>().DisplayTooltip();
                hoverOverObject = null;
            }
        }

        //Play sound on movement
        if ((Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0) && controller.isGrounded && !cloaked) //If there is movement and player is grounded and not cloaked...
        {
            if (!audioPlayerFootstep.isPlaying && audioTimer >= audioMaxTimer) //Sound ring
            {
                if (Input.GetKey(KeyCode.LeftShift) && !crouching)//Running
                {
                    audioTimer = 0;
                    if (footstepCounter == 1)
                    {
                        this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Footstep 1", this.GetComponent<PhotonView>().viewID);
                        footstepCounter = 2;
                    } else
                    {
                        this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Footstep 2", this.GetComponent<PhotonView>().viewID);
                        footstepCounter = 1;
                    }
                }
                else if (!crouching) //If not running or crouching
                {
                    audioTimer = 0;
                    if (footstepCounter == 1)
                    {
                        this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Footstep 1", this.GetComponent<PhotonView>().viewID);
                        footstepCounter = 2;
                    }
                    else
                    {
                        this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Footstep 2", this.GetComponent<PhotonView>().viewID);
                        footstepCounter = 1;
                    }
                } else if (crouching)
                {
                    audioTimer = 0;
                    if (footstepCounter == 1)
                    {
                        this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 5.0f, "Footstep 1", this.GetComponent<PhotonView>().viewID);
                        footstepCounter = 2;
                    }
                    else
                    {
                        this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 5.0f, "Footstep 2", this.GetComponent<PhotonView>().viewID);
                        footstepCounter = 1;
                    }
                }
            }
        }

        //Left Mouse Click
        if (Input.GetMouseButtonDown(0))
        {
            if (hand.transform.childCount != 0)
            {
                if (!(hand.transform.GetChild(0).GetComponent<Weapon>().IsFired))
                {
                    Use();
                }
            }
        }

        //Q key
        if (Input.GetKeyDown(KeyCode.Q))
        {
            this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Whistle", this.GetComponent<PhotonView>().viewID);
        }

        //Cloaking
        if (cloaked)
        {
            StartCoroutine(Cloak(10));
        }
    }

    private void MoveUpdate()
    {
        animator.SetBool("Grounded", controller.isGrounded);

        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        bool run = Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl);

        if (v < 0)
        {
            if (run) { v *= backwardRunScale; }
            else { v *= backwardsWalkScale; }
        }
        else if (run)
        {
            v *= runScale;
        }

        currentV = Mathf.Lerp(currentV, v, Time.deltaTime * interpolation);
        currentH = Mathf.Lerp(currentH, h, Time.deltaTime * interpolation);

        //transform.position += transform.forward * currentV * moveSpeed * Time.deltaTime;
        //transform.position += transform.right * currentH * moveSpeed * Time.deltaTime;

        controller.Move(transform.forward * currentV * moveSpeed * Time.deltaTime);
        controller.Move(transform.right * currentH * moveSpeed * Time.deltaTime);

        //Movement animation
        if (Input.GetAxis("Vertical") != 0)
        {
            animator.SetFloat("MoveSpeed", currentV);
            idleTimer = 0.0f;
        }
        else if (Input.GetAxis("Horizontal") != 0)
        {
            animator.SetFloat("MoveSpeed", currentH);
            idleTimer = 0.0f;
        }
        else //Idle
        {
            animator.SetFloat("MoveSpeed", 0.0f);
            idleTimer += Time.deltaTime;
        }

        //Play sound if idle
        if(idleTimer  > 10.0f && !cloaked)
        {
            this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Breath", this.GetComponent<PhotonView>().viewID);
            idleTimer = 0.0f;
        }

        JumpingAndLanding();

        wasGrounded = controller.isGrounded;
    }

    private void JumpingAndLanding()
    {
        //bool jumpCooldownOver = (Time.time - jumpTimeStamp) >= minJumpInterval;
        
        controller.Move(moveDirection * Time.deltaTime);

        if (controller.isGrounded && Input.GetKey(KeyCode.Space))
        {
            //jumpTimeStamp = Time.time;
            //moveDirection = transform.position;
            moveDirection.y = jumpForce;
            Debug.Log("Jump");
        }

        if(!controller.isGrounded)
            moveDirection.y -= gravity * Time.deltaTime;

        if (!wasGrounded && controller.isGrounded)
        {
            animator.SetTrigger("Land");
            this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, 15.0f, "Footstep 2", this.GetComponent<PhotonView>().viewID);
        }

        if (!controller.isGrounded && wasGrounded)
        {
            animator.SetTrigger("Jump");
        }
    }

    //Creates a sound ring and plays given clip.
    [PunRPC]
    public void Sound(float maxRadius, string clipName, int playerId)
    {
        GameObject player = PhotonView.Find(playerId).gameObject;
        
        GameObject SoundRing = Instantiate(soundRingPrefab, player.transform.position, soundRingPrefab.transform.rotation);
        SoundRing.GetComponent<Ring>().maxRadius = maxRadius;

        if(this.gameObject == GameManager.Instance.LocalPlayer)
        {
            if (maxRadius <= 10.0f)
            {
                SoundRing.GetComponent<Ring>().SetColour(Ring.Colours.Green);
            }
            else
            {
                SoundRing.GetComponent<Ring>().SetColour(Ring.Colours.Yellow);
            }
        } else
        {
            SoundRing.GetComponent<Ring>().SetColour(Ring.Colours.Red);
        }

        switch (clipName)
        {
            case "Footstep 1":
                SoundRing.GetComponent<Ring>().SetSound(footstep1);
                break;
            case "Footstep 2":
                SoundRing.GetComponent<Ring>().SetSound(footstep2);
                break;
            case "Pain":
                SoundRing.GetComponent<Ring>().SetSound(audioPain);
                break;
            case "Death":
                SoundRing.GetComponent<Ring>().SetSound(audioDeath);
                break;
            case "Whistle":
                SoundRing.GetComponent<Ring>().SetSound(audioWhistle);
                break;
            case "Breath":
                SoundRing.GetComponent<Ring>().SetSound(audioBreath);
                break;
        }
        
        
    }

    void Detect()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 4.0f))
        {
            if (hit.collider.tag == "Weapon")
            { //Pick up weapon
                if (hit.collider.gameObject.GetComponent<Weapon>().GetOwner() == null)
                {
                    this.GetComponent<PhotonView>().RPC("PickUp", PhotonTargets.All, hit.collider.gameObject.GetPhotonView().viewID, this.gameObject.GetPhotonView().viewID);

                    GUIManager.Instance.ChangeHeldItem(hit.collider.gameObject.GetComponent<Weapon>().image);
                }
            }
        }
    }

    /*
     * Take an object(weapon) and place it in the parent object(player hand). 
     */
    [PunRPC]
    void PickUp(int objId, int playerId)
    {
        animator.SetTrigger("Pick up");

        GameObject obj;
        GameObject parent;

        obj = PhotonView.Find(objId).gameObject;
        parent = PhotonView.Find(playerId).gameObject;
        obj.transform.GetComponent<Weapon>().SetOwner(parent);

        parent = parent.GetComponent<PlayerControls>().hand;
        parent.transform.position = Vector3.zero;

        obj.transform.parent = parent.transform;
        obj.transform.position = parent.transform.position;
        obj.transform.rotation = parent.transform.rotation;
        obj.GetComponent<Rigidbody>().isKinematic = true;
        obj.GetComponent<BoxCollider>().isTrigger = true;
    }

    /*
     * Remove gameobject from hand.
     */
    [PunRPC]
    void Drop(int playerId)
    {
        animator.SetTrigger("Drop");

        GameObject player;

        player = PhotonView.Find(playerId).gameObject;
        player = player.GetComponent<PlayerControls>().hand;
        
        player.transform.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
        player.transform.GetChild(0).GetComponent<Weapon>().SetOwner(null);
        player.transform.GetChild(0).GetComponent<BoxCollider>().isTrigger = false;
        player.transform.GetChild(0).transform.parent = null;
    }

    /*
     * Use Weapon.
     */
    void Use()
    {
        hand.transform.GetChild(0).GetComponent<Weapon>().UseWeapon();
    }
    

    /*
     * Player crouch down.
     */
    public void Crouch()
    {
        //If not already crouching
        if (!crouching)
        {
            this.gameObject.GetComponent<CharacterController>().height /= 2;
            this.gameObject.GetComponent<CharacterController>().center = new Vector3(0.0f, 0.05f, 0.0f);

            this.gameObject.GetComponent<BoxCollider>().size = new Vector3(this.gameObject.GetComponent<BoxCollider>().size.x, this.gameObject.GetComponent<BoxCollider>().size.y / 2, this.gameObject.GetComponent<BoxCollider>().size.z);
            this.gameObject.GetComponent<BoxCollider>().center = new Vector3(0.0f, 0.05f, 0.0f);

            this.transform.FindChild("Back").GetComponent<BoxCollider>().size = new Vector3(this.transform.FindChild("Back").GetComponent<BoxCollider>().size.x, this.transform.FindChild("Back").GetComponent<BoxCollider>().size.y / 2, this.transform.FindChild("Back").GetComponent<BoxCollider>().size.z);
            this.transform.FindChild("Back").GetComponent<BoxCollider>().center = new Vector3(0.0f, 0.05f, 0.0f);

            Camera.main.transform.localPosition = new Vector3(0.0f, 0.4f, 0.0f);

            animator.SetBool("Crouch", true);

            moveSpeed /= 2;
            jumpForce = 8;
            audioMaxTimer = footstep1.length * 2;
            crouching = true;
        }
    }

    /*
     * Player stand up.
     */
    public void Stand()
    {
        //If not already standing
        if (crouching)
        {
            this.gameObject.GetComponent<CharacterController>().height *= 2;
            this.gameObject.GetComponent<CharacterController>().center = new Vector3(0.0f, 0.55f, 0.0f);

            this.gameObject.GetComponent<BoxCollider>().size = new Vector3(this.gameObject.GetComponent<BoxCollider>().size.x, this.gameObject.GetComponent<BoxCollider>().size.y * 2, this.gameObject.GetComponent<BoxCollider>().size.z);
            this.gameObject.GetComponent<BoxCollider>().center = new Vector3(0.0f, 0.55f, 0.0f);

            this.transform.FindChild("Back").GetComponent<BoxCollider>().size = new Vector3(this.transform.FindChild("Back").GetComponent<BoxCollider>().size.x, this.transform.FindChild("Back").GetComponent<BoxCollider>().size.y * 2, this.transform.FindChild("Back").GetComponent<BoxCollider>().size.z);
            this.transform.FindChild("Back").GetComponent<BoxCollider>().center = new Vector3(0.0f, 0.55f, 0.0f);

            Camera.main.transform.localPosition = new Vector3(0.0f, 1.4f, 0.0f);

            animator.SetBool("Crouch", false);
            
            moveSpeed = storeMoveSpeed;
            jumpForce = storeJumpForce;
            audioMaxTimer = footstep1.length;
            crouching = false;
        }
    }

    [PunRPC]
    void Torch(bool on)
    {
        torch.SetActive(on);
    }

    //Make owner invisible for few seconds.
    public IEnumerator Cloak(float cloakTimer)
    {
        marker.SetActive(false);
        this.photonView.RPC("Visibility", PhotonTargets.All, false);

        yield return new WaitForSeconds(cloakTimer);

        cloaked = false;
        marker.SetActive(true);
        this.photonView.RPC("Visibility", PhotonTargets.All, true);
    }

    [PunRPC]
    public void Visibility(bool visable)
    {
        //As long as this object is not local player, change visibility.
        if(this.gameObject != GameManager.Instance.LocalPlayer)
            this.gameObject.transform.FindChild("Character").FindChild("C_man_1_FBX2013").GetComponent<SkinnedMeshRenderer>().enabled = visable;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting) //Own player
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            /*stream.SendNext(animator.GetFloat("MoveSpeed"));
            stream.SendNext(animator.GetBool("Jump"));
            stream.SendNext(animator.GetBool("Land"));
            stream.SendNext(animator.GetBool("Grounded"));
            stream.SendNext(animator.GetBool("Wave"));
            stream.SendNext(animator.GetBool("Pick up"));
            stream.SendNext(animator.GetBool("Drop"));
            stream.SendNext(animator.GetBool("Crouch"));
            stream.SendNext(animator.GetBool("Stab"));
            stream.SendNext(animator.GetBool("Swing"));
            stream.SendNext(animator.GetBool("Shot"));
            stream.SendNext(animator.GetBool("Die"));*/
        }
        else //Other player
        {
            networkPosition = (Vector3)stream.ReceiveNext();
            networkRotation = (Quaternion)stream.ReceiveNext();
            /*animator.SetFloat("MoveSpeed", (float)stream.ReceiveNext());
            animator.SetBool("Jump", (bool)stream.ReceiveNext());
            animator.SetBool("Land", (bool)stream.ReceiveNext());
            animator.SetBool("Grounded", (bool)stream.ReceiveNext());
            animator.SetBool("Wave", (bool)stream.ReceiveNext());
            animator.SetBool("Pick up", (bool)stream.ReceiveNext());
            animator.SetBool("Drop", (bool)stream.ReceiveNext());
            animator.SetBool("Crouch", (bool)stream.ReceiveNext());
            animator.SetBool("Stab", (bool)stream.ReceiveNext());
            animator.SetBool("Swing", (bool)stream.ReceiveNext());
            animator.SetBool("Shot", (bool)stream.ReceiveNext());
            animator.SetBool("Die", (bool)stream.ReceiveNext());*/
        }
    }
}
