﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Weapon {

    bool isArmed = false;
    public float soundRingRange;
    public Material activeMaterial;

    public override void UseWeapon()
    {
        RaycastHit hit;
        if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 2.0f))
        {
            if(hit.collider.tag == "Wall")
            {
                GUIManager.Instance.ChangeHeldItem(null);
                this.gameObject.GetComponent<PhotonView>().RPC("PlaceMine", PhotonTargets.All, hit.point, hit.normal);
            }
        }
    }

    [PunRPC]
    void PlaceMine(Vector3 point, Vector3 normal)
    {
        owner.GetComponent<PlayerControls>().animator.SetTrigger("Drop");
        this.transform.parent = null;
        this.transform.position = point;
        this.transform.rotation = Quaternion.FromToRotation(Vector3.up, normal);
        this.GetComponent<BoxCollider>().enabled = true;
        StartCoroutine(Timer());
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (isArmed) {
            this.gameObject.GetComponent<ParticleSystem>().Play();

            if (other.tag == "Player" && !hit)
            {
                other.transform.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, other.gameObject.GetPhotonView().viewID);
                this.GetComponent<BoxCollider>().enabled = false;
                hit = true;
                this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, soundRingRange, this.GetComponent<PhotonView>().viewID);
            }
            else if (other.tag == "Player_back" && !hit)
            {
                other.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, other.transform.parent.gameObject.GetPhotonView().viewID);
                this.GetComponent<BoxCollider>().enabled = false;
                hit = true;
                this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, soundRingRange, this.GetComponent<PhotonView>().viewID);
            }

            Destroy(this.gameObject, 1.0f);
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(3);
        this.transform.GetChild(0).gameObject.GetComponent<Renderer>().material = activeMaterial;
        isArmed = true;
    }
}
