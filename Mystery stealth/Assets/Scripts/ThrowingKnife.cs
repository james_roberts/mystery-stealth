﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingKnife : Weapon
{
    public int force;

    [PunRPC]
    public override void PlayAnimation()
    {

    }

    public override void UseWeapon()
    {
        this.GetComponent<PhotonView>().RPC("Throw", PhotonTargets.All);
        GUIManager.Instance.ChangeHeldItem(null);
    }

    [PunRPC]
    void Throw()
    {
        owner.GetComponent<PlayerControls>().animator.SetTrigger("Drop");
        this.gameObject.GetComponent<Rigidbody>().isKinematic = false; //Turn on rigidbody
        this.gameObject.GetComponent<BoxCollider>().isTrigger = false; //Turn on collider
        this.gameObject.transform.parent = null;                       //Remove object from hand

        IsFired = true;
        this.gameObject.GetComponent<Rigidbody>().AddForce(owner.transform.forward * force, ForceMode.Impulse);
        StartCoroutine(Reset()); //Reset weapon for reuse.
    }

    private void OnCollisionEnter(Collision other)
    {
        //If other object is a player, but not the current player, and weapon is being used
        if (other.gameObject.tag == "Player" && other.gameObject != owner && IsFired && !hit)
        {
            hit = true;
            other.gameObject.GetComponent<PlayerHealth>().Damage(damageAmount, other.gameObject.gameObject.GetPhotonView().viewID);
            //other.gameObject.transform.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, other.gameObject.gameObject.GetPhotonView().viewID);
        }
        else if (other.gameObject.tag == "Player_back" && other.gameObject != owner && IsFired && !hit)
        {
            hit = true;
            other.gameObject.GetComponent<PlayerHealth>().Damage(damageAmount, other.gameObject.gameObject.GetPhotonView().viewID);
            //other.gameObject.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, other.gameObject.transform.parent.gameObject.GetPhotonView().viewID);
        }
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(3.0f);

        IsFired = false;
        hit = false;
        owner = null;
    }
}
