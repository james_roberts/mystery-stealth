﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : Photon.MonoBehaviour {

    NetworkManager networkManager;

    public int minPlayerPerGame;

    [Header("Panels")]
    public GameObject mainMenuPanel;
    public GameObject createRoomPanel;
    public GameObject joinRoomPanel;

    [Space(10)]
    public InputField roomTextField;
    public GameObject createRoomButton;
    public GameObject startGameButton;

    [Space(10)]
    public GameObject roomScrollList;
    public GameObject roomButtonPrefab;

    [Space(10)]
    public GameObject playerList;
    public GameObject playerNamePrefab;
    public InputField playerName;

    [Space(10)]
    public Text consoleMessage;

    private void Start()
    {
        networkManager = GameObject.Find("Network Manager").GetComponent<NetworkManager>();

        mainMenuPanel.SetActive(true);
        createRoomPanel.SetActive(false);
        joinRoomPanel.SetActive(false);

        playerName.text = PhotonNetwork.player.NickName;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void CreateRoomMenu()
    {
        //Display Menu
        mainMenuPanel.SetActive(false);
        createRoomPanel.SetActive(true);
        joinRoomPanel.SetActive(false);
    }

    public void JoinRoomMenu()
    {
        //Display Menu
        mainMenuPanel.SetActive(false);
        createRoomPanel.SetActive(false);
        joinRoomPanel.SetActive(true);

        for (int i = roomScrollList.transform.childCount; i > 0; i--) //Clear list of any current rooms listed.
        {
            Debug.Log("Clear player list");
            GameObject.Destroy(roomScrollList.transform.GetChild(i - 1).gameObject);
        }

        //Get list of rooms
        RoomInfo[] roomList = networkManager.GetListOfRooms();

        //Create new button for room and display in panel.
        for (int i = 0; i < roomList.Length; i++)
        {
            GameObject room = (GameObject)Instantiate(roomButtonPrefab, roomScrollList.transform);
            room.transform.localScale = new Vector3(1, 1, 1);
            room.transform.localPosition = new Vector3(roomScrollList.GetComponent<GridLayoutGroup>().preferredWidth, room.transform.localPosition.y, 0.0f);

            string roomName = roomList[i].Name;
            room.GetComponent<Button>().onClick.AddListener(() => JoinRoom(roomName));
            
            room.transform.GetChild(0).GetComponent<Text>().text = roomList[i].Name + "\t" + roomList[i].PlayerCount + "/" + roomList[i].MaxPlayers;
        }
    }

    public void Back()
    {
        //Display Menu
        mainMenuPanel.SetActive(true);
        createRoomPanel.SetActive(false);
        joinRoomPanel.SetActive(false);

        PhotonNetwork.LeaveRoom();
    }

    public void Quit()
    {
        //Quit Game
        Application.Quit();
    }

    public void CreateRoom()
    {
        //Get room name
        string roomName = roomTextField.text;

        if (roomName == "")
            return;

        //Create a room
        bool success = networkManager.CreateRoom(roomName);
        if (!success)
        {
            StartCoroutine(DisplayMessage("Failed to create room as room with given name already exists"));
        } else
        {
            StartCoroutine(DisplayMessage("Created room " + roomName));
            createRoomButton.SetActive(false);
            roomTextField.gameObject.SetActive(false);
        }
    }

    void OnCreatedRoom()
    {
        //Change create button to play
        createRoomButton.SetActive(false);
        startGameButton.SetActive(true);
        roomTextField.gameObject.SetActive(false);
    }

    public void StartGame()
    {
        //Start game with current players (at least two)
        if(PhotonNetwork.room.PlayerCount >= minPlayerPerGame)
        {
            PhotonNetwork.room.IsVisible = false;
            PhotonNetwork.room.IsOpen = false;
            networkManager.StartGame();
        }
    }

    public void JoinRoom(string roomName)
    {
        //Join room
        bool success = networkManager.JoinRoom(roomName);
        if (!success)
        {
            StartCoroutine(DisplayMessage("Failed to join room"));
        } else
        {
            StartCoroutine(DisplayMessage("Joined room " + roomName));
        }

        //Display room menu
        CreateRoomMenu();

        //Only show create buttons if master client
        if (!PhotonNetwork.isMasterClient)
        {
            roomTextField.gameObject.SetActive(false);
            createRoomButton.SetActive(false);
            startGameButton.SetActive(false);
        }
    }

    public void RefreshList()
    {
        //Get list of rooms and rebuild list.
        JoinRoomMenu();

        StartCoroutine(DisplayMessage("Refreshed list"));
    }

    /*
     * Set player's name.
     */
    public void ChangePlayerName()
    {
        if(playerName.text != "")
            PhotonNetwork.playerName = playerName.text;
        else
        {
            networkManager.SetRandomName();
            playerName.text = PhotonNetwork.player.NickName;
        }

        StartCoroutine(DisplayMessage("Changed player name to " + playerName.text));
    }

    /*
     * When local player joins room.
     */
    void OnJoinedRoom()
    {
        Debug.Log("Connected to Room");

        PhotonPlayer[] players = PhotonNetwork.playerList; //Get list of players in room.

        //Fill menu list with players.
        for (int i = 0; i < players.Length; i++)
        {
            GameObject playerText = Instantiate(playerNamePrefab, playerList.transform);
            playerText.transform.localScale = new Vector3(1, 1, 1);
            playerText.transform.localPosition = new Vector3(playerList.transform.localPosition.x, playerText.transform.localPosition.y, 0.0f);
            playerText.GetComponent<Text>().text = "  " + players[i].NickName;
        }
    }

    /*
     * When local player leaves room.
     */
    void OnLeftRoom()
    {
        roomTextField.gameObject.SetActive(true);
        createRoomButton.SetActive(true);
        startGameButton.SetActive(false);

        for(int i = playerList.transform.childCount; i > 0; i--) //Clear list of any current players listed.
        {
            Debug.Log("Clear player list");
            GameObject.Destroy(playerList.transform.GetChild(i - 1).gameObject);
        }
    }

    /*
     * When remote player joins room.
     */
    void OnPhotonPlayerConnected()
    {
        for (int i = playerList.transform.childCount; i > 0; i--) //Clear list of any current players listed.
        {
            Debug.Log("Clear player list");
            GameObject.Destroy(playerList.transform.GetChild(i - 1).gameObject);
        }

        PhotonPlayer[] players = PhotonNetwork.playerList; //Get list of players in room.

        //Fill menu list with players.
        for (int i = 0; i < players.Length; i++)
        {
            GameObject playerText = Instantiate(playerNamePrefab, playerList.transform);
            playerText.transform.localScale = new Vector3(1, 1, 1);
            playerText.transform.localPosition = new Vector3(playerList.transform.localPosition.x, playerText.transform.localPosition.y, 0.0f);
            playerText.GetComponent<Text>().text = "  " + players[i].NickName;
        }
    }

    /*
     * When remote player leaves room.
     */
    void OnPhotonPlayerDisconnected()
    {
        for (int i = playerList.transform.childCount; i > 0; i--) //Clear list of any current players listed.
        {
            Debug.Log("Clear player list");
            GameObject.Destroy(playerList.transform.GetChild(i-1).gameObject);
        }

        PhotonPlayer[] players = PhotonNetwork.playerList; //Get list of players in room.

        //Fill menu list with players.
        for (int i = 0; i < players.Length; i++)
        {
            GameObject playerText = Instantiate(playerNamePrefab, playerList.transform);
            playerText.transform.localScale = new Vector3(1, 1, 1);
            playerText.transform.localPosition = new Vector3(playerList.transform.localPosition.x, playerText.transform.localPosition.y, 0.0f);
            playerText.GetComponent<Text>().text = "  " + players[i].NickName;
        }
    }

    /*
     * When client fails to connect to server.
     */
    void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        consoleMessage.text = "Failed to connect to server";
    }

    void OnConnectionFail(DisconnectCause cause)
    {
        consoleMessage.text = "Lost connection to server: " + cause.ToString();
    }

    /*
     * When master client leaves and changes to another client.
     */
    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        StartCoroutine(DisplayMessage("Master Client changed to " + newMasterClient.NickName));

        if(PhotonNetwork.player == newMasterClient)
        {
            createRoomButton.SetActive(false);
            startGameButton.SetActive(true);
            roomTextField.gameObject.SetActive(false);
        }
    }

    IEnumerator DisplayMessage(string message)
    {
        consoleMessage.text = message;

        yield return new WaitForSecondsRealtime(5.0f);

        consoleMessage.text = "";
    }
}
