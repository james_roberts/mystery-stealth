﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {
    
    private void OnEnable()
    {
        if(this.transform.parent.gameObject.CompareTag("Weapon"))
            this.transform.position = new Vector3(this.transform.parent.position.x, this.transform.parent.position.y + .75f, this.transform.parent.position.z);
        else if (this.transform.parent.gameObject.CompareTag("Player"))
            this.transform.position = new Vector3(this.transform.parent.position.x, this.transform.parent.position.y + 2.75f, this.transform.parent.position.z);

        transform.LookAt(Camera.main.transform);
    }
    //Rotate canvas to face camera.
    void Update()
    {
        transform.LookAt(Camera.main.transform);
    }
}
