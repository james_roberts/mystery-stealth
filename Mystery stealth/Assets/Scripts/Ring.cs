﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour {

    public int segments;
    float radius = 0.1f;
    public float expandRate;
    public float maxRadius;
    LineRenderer line;
    bool circle = true;

    [Header("Colours")]
    public Gradient colourGreen;
    public Gradient colourRed;
    public Gradient colourYellow;

    void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();

        line.numPositions = segments + 1;
        line.useWorldSpace = false;
        
        CreatePoints();
    }

    void Update()
    {
        //Each frame expand the radius of the circle.
        radius += expandRate * Time.deltaTime;
        if (radius <= maxRadius && circle == true)
            CreatePoints();
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void PulseCircle()
    {
        circle = true;
    }

    /*
    Creates a circle by setting points, each a slightly different angle than the last until they reconnect.
    */
    void CreatePoints()
    {
        float x;
        float y;
        float z = 0f;

        float angle = 0f;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle);
            y = Mathf.Cos(Mathf.Deg2Rad * angle);
            line.SetPosition(i, new Vector3(x, y, z) * radius);
            angle += (360f / segments);
        }
    }

    public void SetSound(AudioClip clip)
    {
        this.gameObject.GetComponent<AudioSource>().clip = clip;
        this.gameObject.GetComponent<AudioSource>().Play();
    }

    public enum Colours {Green, Red, Yellow};

    public void SetColour(Colours colour)
    {
        switch (colour)
        {
            case Colours.Green:
                gameObject.GetComponent<LineRenderer>().colorGradient = colourGreen;
                break;
            case Colours.Red:
                gameObject.GetComponent<LineRenderer>().colorGradient = colourRed;
                break;
            case Colours.Yellow:
                gameObject.GetComponent<LineRenderer>().colorGradient = colourYellow;
                break;
        }
        //Line varaiable not assigned when called.
    }
}
