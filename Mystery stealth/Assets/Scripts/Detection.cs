using System.Collections;
using UnityEngine;

public class Detection : MonoBehaviour
{
	public float reach = 4.0F;
	[HideInInspector] public bool inReach;

	void Update ()
	{
		//Set origin of ray to 'center of screen' and direction of ray to 'cameraview'.
		Ray ray = Camera.main.ViewportPointToRay(new Vector3 (0.5F, 0.5F, 0F));

		RaycastHit hit;

		//Cast ray from center of the screen towards where the player is looking.
		if (Physics.Raycast(ray, out hit, reach))
		{
			if (hit.collider.tag == "Door")
			{
				inReach = true;

				if (Input.GetMouseButton(1))
				{
					//Get access to the 'Door' script attached to the object that was hit.
					Door doorScript = hit.transform.gameObject.GetComponent<Door>();

                    //Open/close the door by running the 'Open' function found in the 'Door' script.
                    if (doorScript.isRunning == false)
                        hit.collider.GetComponent<PhotonView>().RPC("Open", PhotonTargets.All);
				}
			}
			else
                inReach = false;
		}
		else
            inReach = false;
	}
}
