﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerSetup : Photon.MonoBehaviour {

    public GameObject playerCanvas;
    public Text playerNameText;

    [SerializeField]
    Behaviour[] components;

    private void Start()
    {
        //Disable components if object not belong to client
        if (!photonView.isMine)
        {
            for(int i = 0; i < components.Length; i++)
            {
                components[i].enabled = false;
            }
        } else //Disable if object does belong to client
        {
            playerCanvas.SetActive(false);
        }

        //Display player name on UI
        playerNameText.text = photonView.owner.NickName;
    }
}
