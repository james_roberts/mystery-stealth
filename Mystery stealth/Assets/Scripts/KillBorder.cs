﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillBorder : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("KillBorder " + other.name);

        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, 10, other.gameObject.GetPhotonView().viewID);
        } else
        {
            Destroy(other.gameObject);
        }
    }
}
