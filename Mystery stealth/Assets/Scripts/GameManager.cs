﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : Photon.MonoBehaviour {

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    [Header("Prefabs")]
    public GameObject playerPrefab;
    public GameObject deadPlayerPrefab;
    public GameObject[] spawnPosition;

    [Header("UI")]
    public Image startGameOverlay;

    [Header("End Game")]
    public Image endMessage;
    public Sprite winImage;
    public Sprite loseImage;
    public GameObject endGameMenu;
    public GameObject leaveButton;

    int numOfDeadPlayers = 0;
    int numOfPlayers;
    bool isAlive = true; //Is this local player alive.

    //The GameObject belonging to the local client
    GameObject localPlayer;
    public GameObject LocalPlayer
    {
        get
        {
            return localPlayer;
        }
    }
    
    void Start()
    {
        instance = this;

        //Spawn player object for client on all clients, storing a reference of object.
        localPlayer = PhotonNetwork.Instantiate(playerPrefab.name, spawnPosition[PhotonNetwork.player.ID - 1].transform.position, Quaternion.identity, 0);
        numOfPlayers = PhotonNetwork.playerList.Length;

        StartCoroutine(FadeOutOverlay());
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    /*
     * Kills player who's health has reached zero. Called over network.
     */
    public void Death(GameObject player)
    {
        numOfDeadPlayers++;
        player.GetComponent<Animator>().SetBool("Die", true);

        //If client is the dead player...
        if (player.GetPhotonView().isMine)
        {
            //Spawn new player controls for dead player.
            Instantiate(deadPlayerPrefab, player.transform.position, Quaternion.identity);
            isAlive = false;
        }
        
        Destroy(player);

        if (numOfDeadPlayers == numOfPlayers - 1)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            endMessage.gameObject.SetActive(true);

            localPlayer.GetComponent<PlayerControls>().canControl = false;
            if(GameObject.FindGameObjectWithTag("Spectator") != null)
                GameObject.FindGameObjectWithTag("Spectator").GetComponent<SpectatorControls>().canControl = false;

            if (isAlive) //If this player is still alive show win message.
            {
                endMessage.sprite = winImage;
            } else //Otherwise show lose message.
            {
                endMessage.sprite = loseImage;
            }

            if (PhotonNetwork.isMasterClient)
            {
                endGameMenu.SetActive(true);
                leaveButton.SetActive(true);
            } else
            {
                endGameMenu.SetActive(true);
                leaveButton.SetActive(true);
            }
        }
    }

    void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel(0);
    }

    [PunRPC]
    void DeletePlayer(int playerId)
    {
        GameObject player = PhotonView.Find(playerId).gameObject;

        //Take away player from counter
        numOfPlayers--;

        //Delete player object
        Destroy(player);
    }

    public IEnumerator FadeOutOverlay()
    {
        Color c;
        c = startGameOverlay.color;
        c.a = 1.0f;
        startGameOverlay.color = c;

        

        while (startGameOverlay.color.a > 0.0f) //If overlay is not invisible
        {
            c.a -= 0.005f;
            startGameOverlay.color = c;
            yield return new WaitForSeconds(0.005f);
        }
        GameManager.Instance.LocalPlayer.GetComponent<PlayerControls>().canControl = true;
    }
}
