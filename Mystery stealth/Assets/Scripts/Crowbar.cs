﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crowbar : Weapon
{
    [PunRPC]
    public override void PlayAnimation()
    {
        //this.transform.parent.GetComponent<Animator>().SetTrigger("Use Swing");
        owner.GetComponent<Animator>().SetTrigger("Swing");
    }

    public override void UseWeapon()
    {
        IsFired = true;
        
        this.gameObject.GetComponent<PhotonView>().RPC("PlayAnimation", PhotonTargets.All);

        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        Debug.Log("Use Crowbar");
        yield return new WaitForSeconds(1.0f);

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 4.0f))
        {
            if (hit.collider.tag == "Player")
            {
                hit.collider.transform.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.gameObject.GetPhotonView().viewID);
            }
            else if (hit.collider.tag == "Player_back")
            {
                hit.collider.transform.parent.GetComponent<PhotonView>().RPC("Damage", PhotonTargets.All, damageAmount, hit.collider.transform.parent.gameObject.GetPhotonView().viewID);
            }
        }
    }
}
