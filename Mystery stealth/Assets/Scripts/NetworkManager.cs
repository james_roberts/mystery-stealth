﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour {

    public float version;

    //private const string roomName = "Room1";
    //private RoomInfo[] roomsList;
    private TypedLobby lobbyName = new TypedLobby("New_Lobby", LobbyType.Default);

    public GameObject playerPrefab;
    //public Vector3 spawnPosition;
    char[] characters = {'a','b', 'c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9' };

    // Use this for initialization
    void Start () {
        PhotonNetwork.automaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings("" + version);
        DontDestroyOnLoad(transform.gameObject);

        SetRandomName();
    }

    void OnConnectedToMaster()
    {
        Debug.Log("Conencted to " + PhotonNetwork.Server);
        PhotonNetwork.JoinLobby(lobbyName);
    }

    void OnJoinedLobby()
    {
        Debug.Log("Joined Lobby");
    }

    /*
     * Creates a new room with given name, returns true if room was created, false if already exists. 
     */
    public bool CreateRoom(string roomName)
    {
        RoomOptions roomOptions = new RoomOptions() { MaxPlayers = 3, IsOpen = true, IsVisible = true };
        return PhotonNetwork.CreateRoom(roomName, roomOptions, TypedLobby.Default);
    }

    /*
     * Joins a existing room, returns true if joined, false if failed.
     */
    public bool JoinRoom(string roomName)
    {
        return PhotonNetwork.JoinRoom(roomName);
    }

    /*
     * Returns array list of existing rooms.
     */
    public RoomInfo[] GetListOfRooms()
    {
        return PhotonNetwork.GetRoomList();
    }

    /*
     * Loads game scene for all connected users and starts game.
     */
    public void StartGame()
    {
        PhotonNetwork.LoadLevel("Prototype");
    }

    /*
     * Creates a random name for player.
     */
    public void SetRandomName()
    {
        string playerName = "";
        for (int i = 0; i < 8; i++)
        {
            playerName += characters[Random.Range(0, characters.Length)];
        }
        PhotonNetwork.playerName = playerName;
    }
}
