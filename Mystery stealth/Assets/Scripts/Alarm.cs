﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : Weapon {

    bool isArmed = false;
    public float soundRingRange;
    public Material activeMaterial;

    [PunRPC]
    public override void PlayAnimation()
    {
    }

    public override void UseWeapon()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 4.0f))
        {
            if (hit.collider.tag == "Wall")
            {
                GUIManager.Instance.ChangeHeldItem(null);
                this.gameObject.GetComponent<PhotonView>().RPC("PlaceMine", PhotonTargets.All, hit.point, hit.normal);
            }
        }
    }

    [PunRPC]
    void PlaceMine(Vector3 point, Vector3 normal)
    {
        owner.GetComponent<PlayerControls>().animator.SetTrigger("Drop");
        this.transform.parent = null;
        this.transform.position = point;
        this.transform.rotation = Quaternion.FromToRotation(Vector3.up, normal);
        this.GetComponent<BoxCollider>().enabled = true;
        StartCoroutine(Timer());
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (isArmed)
        {
            if ((other.tag == "Player" || other.tag == "Player_back") && !hit)
            {
                this.GetComponent<BoxCollider>().enabled = false;
                hit = true;
                StartCoroutine(PlayAlarm());
            }

            Destroy(this.gameObject, clip.length * 4);
        }
    }

    IEnumerator PlayAlarm()
    {
        int i = 0;
        do
        {
            this.GetComponent<PhotonView>().RPC("Sound", PhotonTargets.All, soundRingRange, this.GetComponent<PhotonView>().viewID);
            yield return new WaitForSeconds(clip.length);
            i++;
        } while (i < 4);
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(3);
        this.transform.GetChild(0).gameObject.GetComponent<Renderer>().material = activeMaterial;
        isArmed = true;
    }
}
