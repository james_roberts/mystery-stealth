﻿using UnityEngine;
using System.Collections.Generic;

public class SimpleCharacterControl : Photon.MonoBehaviour {

    private enum ControlMode
    {
        Tank,
        Direct
    }

    private CharacterController controller;

    [SerializeField] private float m_moveSpeed = 2;
    [SerializeField] private float m_jumpForce = 4;
    [SerializeField] private float m_gravity = 20.0F;
    [SerializeField] private Animator m_animator;

    [SerializeField] private ControlMode m_controlMode = ControlMode.Direct;

    private float m_currentV = 0;
    private float m_currentH = 0;

    private readonly float m_interpolation = 10;
    private readonly float m_runScale = 1.66f;
    private readonly float m_backwardsWalkScale = 0.26f;
    private readonly float m_backwardRunScale = 0.66f;

    private bool m_wasGrounded;
    private Vector3 m_currentDirection = Vector3.zero;
    private Vector3 moveDirection = Vector3.zero;

    private float m_jumpTimeStamp = 0;
    private float m_minJumpInterval = 0.25f;

    public GameObject hand;

    //private bool m_isGrounded;

    /*private void OnCollisionEnter(Collision collision)
    {
        ContactPoint[] contactPoints = collision.contacts;
        for(int i = 0; i < contactPoints.Length; i++)
        {
            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
            {
                if (!m_collisions.Contains(collision.collider)) {
                    m_collisions.Add(collision.collider);
                }
                m_isGrounded = true;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        ContactPoint[] contactPoints = collision.contacts;
        bool validSurfaceNormal = false;
        for (int i = 0; i < contactPoints.Length; i++)
        {
            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
            {
                validSurfaceNormal = true; break;
            }
        }

        if(validSurfaceNormal)
        {
            m_isGrounded = true;
            if (!m_collisions.Contains(collision.collider))
            {
                m_collisions.Add(collision.collider);
            }
        } else
        {
            if (m_collisions.Contains(collision.collider))
            {
                m_collisions.Remove(collision.collider);
            }
            if (m_collisions.Count == 0) { m_isGrounded = false; }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(m_collisions.Contains(collision.collider))
        {
            m_collisions.Remove(collision.collider);
        }
        if (m_collisions.Count == 0) { m_isGrounded = false; }
    }*/

    private void Start()
    {
        controller = this.gameObject.GetComponent<CharacterController>();
    }

    void Update () {
        m_animator.SetBool("Grounded", controller.isGrounded);

        switch(m_controlMode)
        {
            case ControlMode.Direct:
                DirectUpdate();
                break;

            case ControlMode.Tank:
                TankUpdate();
                break;

            default:
                Debug.LogError("Unsupported state");
                break;
        }

        m_wasGrounded = controller.isGrounded;
    }

    private void TankUpdate()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        bool run = Input.GetKey(KeyCode.LeftShift);

        if (v < 0) {
            if (run) { v *= m_backwardRunScale; }
            else { v *= m_backwardsWalkScale; }
        } else if(run)
        {
            v *= m_runScale;
        }

        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        transform.position += transform.forward * m_currentV * m_moveSpeed * Time.deltaTime;
        transform.position += transform.right * m_currentH * m_moveSpeed * Time.deltaTime;
        //transform.Rotate(0, m_currentH * m_turnSpeed * Time.deltaTime, 0);

        if (Input.GetAxis("Vertical") != 0)
            m_animator.SetFloat("MoveSpeed", m_currentV);
        else if (Input.GetAxis("Horizontal") != 0)
            m_animator.SetFloat("MoveSpeed", m_currentH);

        //E key
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (hand.transform.childCount == 0) //If not holding weapon
            {
                Detect();
            }
            else //If holding weapon
            {
                this.GetComponent<PhotonView>().RPC("Drop", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
            }
        }


        if (Input.GetKeyDown(KeyCode.Q))
            m_animator.SetBool("Pickup", true);
        if (Input.GetKeyUp(KeyCode.Q))
            m_animator.SetBool("Pickup", false);

        JumpingAndLanding();
    }

    private void DirectUpdate()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        Transform camera = Camera.main.transform;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            v *= m_runScale;
            h *= m_runScale;
        }

        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        Vector3 direction = camera.forward * m_currentV + camera.right * m_currentH;

        float directionLength = direction.magnitude;
        direction.y = 0;
        direction = direction.normalized * directionLength;

        if(direction != Vector3.zero)
        {
            m_currentDirection = Vector3.Slerp(m_currentDirection, direction, Time.deltaTime * m_interpolation);

            transform.rotation = Quaternion.LookRotation(m_currentDirection);
            transform.position += m_currentDirection * m_moveSpeed * Time.deltaTime;

            m_animator.SetFloat("MoveSpeed", direction.magnitude);
        }

        JumpingAndLanding();
    }

    private void JumpingAndLanding()
    {
        bool jumpCooldownOver = (Time.time - m_jumpTimeStamp) >= m_minJumpInterval;

        if (jumpCooldownOver && controller.isGrounded && Input.GetKey(KeyCode.Space))
        {
            m_jumpTimeStamp = Time.time;
            moveDirection.y = m_jumpForce;
        }

        moveDirection.y -= m_gravity * Time.deltaTime;
        this.gameObject.GetComponent<CharacterController>().Move(moveDirection * Time.deltaTime);

        if (!m_wasGrounded && controller.isGrounded)
        {
            m_animator.SetTrigger("Land");
        }

        if (!controller.isGrounded && m_wasGrounded)
        {
            m_animator.SetTrigger("Jump");
        }
    }

    void Detect()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 2))
        {
            Debug.Log(hit.collider.name);
            Debug.Log(hand);
            if (hit.collider.tag == "Weapon")
            { //Pick up weapon
                if (hit.collider.gameObject.GetComponent<Weapon>().GetOwner() == null)
                    this.GetComponent<PhotonView>().RPC("PickUp", PhotonTargets.All, hit.collider.gameObject.GetPhotonView().viewID, this.gameObject.GetPhotonView().viewID);
            }
        }
    }
}
